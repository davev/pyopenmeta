README for pyOpenMeta
=====================

Leverages the xattr (http://pypi.python.org/pypi/xattr) and biplist 
(http://pypi.python.org/pypi/biplist) libraries to enable read and write access
to OpenMeta (http://code.google.com/p/openmeta/) tags on OS X.

This software is released under the MIT open source license, see LICENSE.txt for 
details.


