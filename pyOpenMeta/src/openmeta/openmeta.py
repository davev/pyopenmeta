# -*- coding: utf-8 -*-
# This work was created by Dave Vieglais of the University of Kansas
#
#   Copyright 2012 Dave Vieglais, University of Kansas
#
# Licensed under the MIT License (the "License").
# You may not use this file except in compliance with the License.
# A full copy of the License should be included with this project in the file
#   LICENSE.txt 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
# THE SOFTWARE.

'''
Created on Dec 2, 2012

'''

import sys
import os
import logging
import xattr
import zlib
import biplist
import datetime


def _uniquify(seq):
  '''Utility to return a list containing only unique values in seq
  http://www.peterbe.com/plog/uniqifiers-benchmark
  '''
  seen = set()
  seen_add = seen.add
  return [ x for x in seq if x not in seen and not seen_add(x)]


def getBinaryProperty(fname, attr):
  '''
  '''
  try:
    value = xattr.getxattr(fname, attr)
  except IOError as e:
    logging.debug(e)
    return []
  try:
    attr = zlib.decompress(attr)
  except zlib.error:
    pass
  return biplist.readPlistFromString(value)


def getFileOpenMetaTags(fname):
  '''
  Returns a list of OpenMeta tags for the specified file. An empty list is 
  returned if there are no tags.
  
  @param fname (string) The name of the file for which to retrieve tags
  
  @return string[] A possibly empty list of OpenMeta tags for the file
  '''
  return getBinaryProperty(fname, "org.openmetainfo:kMDItemOMUserTags")


def setFileOpenMetaTags(fname, tags):
  '''
  Applies the specified tags to a file. Existing tags will be overwritten. The
  OpenMeta time stamp is updated to the current time.
  
  @param fname (string) Name of file that will receive the list of tags
  @param tags (string[]) List of strings that will be set as OpenMeta tags
  '''
  tags = _uniquify(tags)
  t = datetime.datetime.now()
  #Apparently kMDItemOMUserTags is the current preference
  attrs = {'com.apple.metadata:kMDItemOMUserTagTime':t,
           'org.openmetainfo.time:kMDItemOMUserTagTime': t,
           'org.openmetainfo.time:kMDItemOMUserTags': t,
           'org.openmetainfo:kMDItemOMUserTagTime': t,
           'com.apple.metadata:kMDItemOMUserTags':tags,
           'com.apple.metadata:kOMUserTags':tags,
           'org.openmetainfo:kMDItemOMUserTags': tags,
           'org.openmetainfo:kOMUserTags': tags}
  for attr in attrs:
    bp = biplist.writePlistToString(attrs[attr])
    xattr.setxattr(fname, attr, bp)


def addOpenMetaFileTags(fname, tag):
  '''Adds a tag to a file, preserving existing tags if present.
  
  @param fname (string) Name of file to receive tag
  @param tag (string or string[]) A string or a list of strings that represent
    tag value(s) to append to existing tags for the file.
  '''
  tags = getFileOpenMetaTags(fname)
  if isinstance(basestring, tag):
    tag = [tag, ]
  for atag in tag:
    if atag not in tags:
      tags.append(atag)
  return setFileOpenMetaTags(fname, tags)


if __name__ == "__main__":
  import os
  ftest = os.path.abspath(sys.argv[1])
  tags = getFileOpenMetaTags(ftest)
  print ", ".join(tags)
  
  setFileOpenMetaTags(ftest, ['dave','openmeta'])

  tags = getFileOpenMetaTags(ftest)
  print ", ".join(tags)

