#! /bin/env python

# This work was created by Dave Vieglais of the University of Kansas
#
#   Copyright 2012 Dave Vieglais, University of Kansas
#
# Licensed under the MIT License (the "License").
# You may not use this file except in compliance with the License.
# A full copy of the License should be included with this project in the file
#   LICENSE.txt 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
# THE SOFTWARE.

'''
Script to manipulate OpenMeta tags.

pyopenmeta [-r] [-a TAG0,TAG1,...] [-t TAG0,TAG1,...] PATH

  Get or set OpenMeta tags on PATH. If PATH is a folder then applies to all
  files in folder. Specifying -r make the operation recursive. Tags are 
  specified as a comma delimited list of values.
  
  -r   Apply operation recursively in PATH if a folder
  TODO: -a   Add TAG0, TAG1, ... to file or files if a folder 
  TODO: -t   Replace tags with TAG0,TAG1,... for file or files
'''

import os
import logging
import argparse
import glob
from openmeta import openmeta

def _addTagsToDict(tags, alltags={}):
  for tag in tags:
    try:
      alltags[tag] += 1
    except KeyError as e:
      alltags[tag] = 1
  return alltags


def getTags(path, recurse=False, alltags={}):
  '''Retrieve a list of tags and their ocourence from the specified path 
  '''
  logging.debug("path = %s" % path)
  if os.path.isfile(path):
    tags = openmeta.getFileOpenMetaTags(path)
    return _addTagsToDict(tags, alltags)

  if not recurse:
    for f in os.listdir(path):
      if os.path.isfile(os.path.join(path, f)):
        tags = openmeta.getFileOpenMetaTags(path)
        alltags = _addTagsToDict(tags, alltags)
    return alltags
    
  for root, dirs, files in os.walk(path):
    for f in files:
      fpath = os.path.join(root, f)
      logging.debug("F Path = %s" % fpath)
      tags = openmeta.getFileOpenMetaTags(fpath)
      alltags = _addTagsToDict(tags, alltags)
  
  return alltags


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Get or set OpenMeta tags.')
  parser.add_argument('path', type=unicode,
                       help='Path to file or folder')
  parser.add_argument('-r', '--recurse', action='store_true', default=False,
                      help='Recurse into subfolders')
  parser.add_argument('-v', '--verbosity', type=int, default=1,
                      help='Verbosity level, 1-5')
  args = parser.parse_args()
  llog = args.verbosity*10
  if llog < 10:
    llog = 10
  if llog > 50:
    llog = 50
  logging.basicConfig(level=args.verbosity*10)
  tags = getTags(args.path, recurse=args.recurse, alltags={})
  for tag in tags.keys():
    print "%s [%d]" % (tag, tags[tag])

    