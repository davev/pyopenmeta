# This work was created by Dave Vieglais of the University of Kansas
#
#   Copyright 2012 Dave Vieglais, University of Kansas
#
# Licensed under the MIT License (the "License").
# You may not use this file except in compliance with the License.
# A full copy of the License should be included with this project in the file
#   LICENSE.txt 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
# THE SOFTWARE.

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pyOpenMeta",
    version = "0.0.1",
    author = "Dave Vieglais",
    author_email = "dave.vieglais@gmail.com",
    description = ("Library for reading and writing OpenMeta xattr tags "
                   "(http://code.google.com/p/openmeta/) for OS X."),
    license = "MIT",
    keywords = "tags tagging openmeta",
    url = "http://bits.vieglais.com/pyopenmeta",
    packages=['pyopenmeta'],
    long_description=read('../README.txt'),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
    ],
)